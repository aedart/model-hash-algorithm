<?php  namespace Aedart\Model\Hash\Algorithm\Traits; 

use Aedart\Model\Hash\Algorithm\Exceptions\InvalidHashAlgorithmException;
use Aedart\Model\Hash\Algorithm\Validators\SupportedHashAlgorithmListValidator;

/**
 * Trait Supported Hash Algorithm List
 *
 * @see SupportedHashAlgorithmListAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Hash\Algorithm\Traits
 */
trait SupportedHashAlgorithmListTrait {

    /**
     * Supported hashing algorithms
     *
     * @var string[]
     */
    protected $supportedHashAlgorithmList = [];

    /**
     * Eventual last error message, for a given list, in case
     * that it has not passed validation
     *
     * @var string|null
     */
    protected $_invalidSupportedHashAlgorithmListErrorMessage = null;

    /**
     * Set the list of supported hashing algorithms
     *
     * @param string[] $list [Optional][Default array()] List of supported hashing algorithms - empty list is
     *                       valid, because it's the same as stating that "no hashing algorithms are supported"
     *
     * @return void
     *
     * @throws InvalidHashAlgorithmException If provided list contains invalid entries
     */
    public function setSupportedHashAlgorithmList(array $list = []){
        if(!$this->isSupportedHashAlgorithmListValid($list)){
            throw new InvalidHashAlgorithmException($this->_invalidSupportedHashAlgorithmListErrorMessage);
        }
        $this->supportedHashAlgorithmList = $list;
    }

    /**
     * Get the list of supported hashing algorithms
     *
     * If no list has been specified, this method sets and returns
     * a default list of supported hashing algorithms
     *
     * @see getDefaultSupportedHashAlgorithmList()
     *
     * @return string[] List of supported hashing algorithms
     */
    public function getSupportedHashAlgorithmList(){
        if(!$this->hasSupportedHashAlgorithmList() && $this->hasDefaultSupportedHashAlgorithmList()){
            $this->setSupportedHashAlgorithmList($this->getDefaultSupportedHashAlgorithmList());
        }
        return $this->supportedHashAlgorithmList;
    }

    /**
     * <b>Override</b>
     *
     * Returns a list of registered hashing algorithms - <b>system dependent!</b>
     *
     * @see http://php.net/manual/en/function.hash-algos.php
     *
     * @return string[] A default list of supported hashing algorithms - empty array if none are supported
     */
    public function getDefaultSupportedHashAlgorithmList(){
        return hash_algos();
    }

    /**
     * Check if a list of supported hashing algorithms was set
     *
     * @return bool True if a list was set, false if not
     */
    public function hasSupportedHashAlgorithmList(){
        return !empty($this->supportedHashAlgorithmList);
    }

    /**
     * Check if a default list of supported hashing algorithms is available
     *
     * @return bool True if a default list is available, false if not
     */
    public function hasDefaultSupportedHashAlgorithmList(){
        $default = $this->getDefaultSupportedHashAlgorithmList();
        return !empty($default);
    }

    /**
     * Check if the list contains valid non-empty strings (algorithm names)
     *
     * @param array $list [Optional][Default array()] List to be validated
     *
     * @return bool True if each list entry is a valid non-empty string, false if not.
     *              <b>Empty lists are valid</b>, because it's the same as stating that "no
     *              hashing algorithms are supported"
     */
    public function isSupportedHashAlgorithmListValid(array $list = []){
        $result = SupportedHashAlgorithmListValidator::isValid($list);
        $this->_invalidSupportedHashAlgorithmListErrorMessage = SupportedHashAlgorithmListValidator::getLastErrorMessage();
        return $result;
    }

}