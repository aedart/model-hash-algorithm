<?php  namespace Aedart\Model\Hash\Algorithm\Traits; 

use Aedart\Model\Hash\Algorithm\Exceptions\InvalidHashAlgorithmException;
use Aedart\Model\Hash\Algorithm\Validators\HashAlgorithmNameValidator;

/**
 * Trait Hash Algorithm (name)
 *
 * @see HashAlgorithmAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Hash\Algorithm\Traits
 */
trait HashAlgorithmTrait {

    /**
     * Hash algorithm name
     *
     * @var string|null
     */
    protected $hashAlgorithm = null;

    /**
     * Eventual last error message, if a given algorithm name
     * is invalid
     *
     * @var string|null
     */
    protected $_invalidHashAlgorithmErrorMessage = null;

    /**
     * Set the hashing algorithm name
     *
     * @param string $name Hashing algorithm name
     *
     * @return void
     *
     * @throws InvalidHashAlgorithmException If given algorithm name is not accepted or supported
     */
    public function setHashAlgorithm($name){
        if(!$this->isHashAlgorithmValid($name)){
            throw new InvalidHashAlgorithmException($this->_invalidHashAlgorithmErrorMessage);
        }
        $this->hashAlgorithm = $name;
    }

    /**
     * Get the hashing algorithm name
     *
     * If no hashing algorithm name has been set, this method
     * sets and returns a default hashing algorithm name, if
     * any is available
     *
     * @see getDefaultHashAlgorithm()
     *
     * @return string|null This components hashing algorithm (name) or null if none has been set
     */
    public function getHashAlgorithm(){
        if(!$this->hasHashAlgorithm() && $this->hasDefaultHashAlgorithm()){
            $this->setHashAlgorithm($this->getDefaultHashAlgorithm());
        }
        return $this->hashAlgorithm;
    }

    /**
     * Get a default hashing algorithm name
     *
     * @return string|null A default hashing algorithm name or null if none is available
     */
    public function getDefaultHashAlgorithm(){
        return null;
    }

    /**
     * Check if a hashing algorithm name has been set
     *
     * @return bool True if a hashing algorithm name has been set, false if not
     */
    public function hasHashAlgorithm(){
        if(!is_null($this->hashAlgorithm)){
            return true;
        }
        return false;
    }

    /**
     * Check if a default hashing algorithm is available
     *
     * @return bool True if a default hashing algorithm name is available, false if not
     */
    public function hasDefaultHashAlgorithm(){
        if(!is_null($this->getDefaultHashAlgorithm())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given hashing algorithm name is accepted or supported
     * by this component
     *
     * @param mixed $name The hashing algorithm name to be validated
     *
     * @return bool True if given name is an accepted or supported hash algorithm name, false if not
     */
    public function isHashAlgorithmValid($name){
        $result = HashAlgorithmNameValidator::isValid($name);
        $this->_invalidHashAlgorithmErrorMessage = HashAlgorithmNameValidator::getLastErrorMessage();
        return $result;
    }

}