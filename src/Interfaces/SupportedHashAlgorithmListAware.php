<?php  namespace Aedart\Model\Hash\Algorithm\Interfaces;
use Aedart\Model\Hash\Algorithm\Exceptions\InvalidHashAlgorithmException;

/**
 * Interface Supported Hash Algorithm List Aware
 *
 * Components, classes or objects that implements this interface, promise that list of supported
 * hashing algorithm names can be specified and retrieved.
 *
 * Furthermore, depending upon implementation, a default list might be returned, if
 * none has been set prior to obtaining it.
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Hash\Algorithm\Interfaces
 */
interface SupportedHashAlgorithmListAware {

    /**
     * Set the list of supported hashing algorithms
     *
     * @param string[] $list [Optional][Default array()] List of supported hashing algorithms - empty list is
     *                       valid, because it's the same as stating that "no hashing algorithms are supported"
     *
     * @return void
     *
     * @throws InvalidHashAlgorithmException If provided list contains invalid entries
     */
    public function setSupportedHashAlgorithmList(array $list = []);

    /**
     * Get the list of supported hashing algorithms
     *
     * If no list has been specified, this method sets and returns
     * a default list of supported hashing algorithms
     *
     * @see getDefaultSupportedHashAlgorithmList()
     *
     * @return string[] List of supported hashing algorithms
     */
    public function getSupportedHashAlgorithmList();

    /**
     * Get a list of default supported hashing algorithms
     *
     * @return string[] A default list of supported hashing algorithms - empty array if none are supported
     */
    public function getDefaultSupportedHashAlgorithmList();

    /**
     * Check if a list of supported hashing algorithms was set
     *
     * @return bool True if a list was set, false if not
     */
    public function hasSupportedHashAlgorithmList();

    /**
     * Check if a default list of supported hashing algorithms is available
     *
     * @return bool True if a default list is available, false if not
     */
    public function hasDefaultSupportedHashAlgorithmList();

    /**
     * Check if the list contains valid non-empty strings (algorithm names)
     *
     * @param array $list [Optional][Default array()] List to be validated
     *
     * @return bool True if each list entry is a valid non-empty string, false if not.
     *              <b>Empty lists are valid</b>, because it's the same as stating that "no
     *              hashing algorithms are supported"
     */
    public function isSupportedHashAlgorithmListValid(array $list = []);
}