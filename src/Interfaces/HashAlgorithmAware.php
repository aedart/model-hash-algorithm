<?php  namespace Aedart\Model\Hash\Algorithm\Interfaces;

use Aedart\Model\Hash\Algorithm\Exceptions\InvalidHashAlgorithmException;

/**
 * Interface Hash Algorithm (name) Aware
 *
 * Components, classes or objects that implements this interface, promise that a given hashing algorithm
 * name can be specified and retrieved.
 * Furthermore, depending upon implementation, a default hashing algorithm name might be returned, if
 * none has been set prior to obtaining it.
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Hash\Algorithm\Interfaces
 */
interface HashAlgorithmAware {

    /**
     * Set the hashing algorithm name
     *
     * @param string $name Hashing algorithm name
     *
     * @return void
     *
     * @throws InvalidHashAlgorithmException If given algorithm name is not accepted or supported
     */
    public function setHashAlgorithm($name);

    /**
     * Get the hashing algorithm name
     *
     * If no hashing algorithm name has been set, this method
     * sets and returns a default hashing algorithm name, if
     * any is available
     *
     * @see getDefaultHashAlgorithm()
     *
     * @return string|null This components hashing algorithm (name) or null if none has been set
     */
    public function getHashAlgorithm();

    /**
     * Get a default hashing algorithm name
     *
     * @return string|null A default hashing algorithm name or null if none is available
     */
    public function getDefaultHashAlgorithm();

    /**
     * Check if a hashing algorithm name has been set
     *
     * @return bool True if a hashing algorithm name has been set, false if not
     */
    public function hasHashAlgorithm();

    /**
     * Check if a default hashing algorithm is available
     *
     * @return bool True if a default hashing algorithm name is available, false if not
     */
    public function hasDefaultHashAlgorithm();

    /**
     * Check if the given hashing algorithm name is accepted or supported
     * by this component
     *
     * @param mixed $name The hashing algorithm name to be validated
     *
     * @return bool True if given name is an accepted or supported hash algorithm name, false if not
     */
    public function isHashAlgorithmValid($name);
}