<?php  namespace Aedart\Model\Hash\Algorithm\Exceptions; 

/**
 * Class Invalid Hash Algorithm Exception
 *
 * Throw this exception when an invalid hashing algorithm has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Hash\Algorithm\Exceptions
 */
class InvalidHashAlgorithmException extends \InvalidArgumentException{

}