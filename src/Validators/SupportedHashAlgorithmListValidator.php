<?php  namespace Aedart\Model\Hash\Algorithm\Validators;

use Aedart\Validate\BaseValidator;
use Aedart\Validate\String\NonEmptyStringValidator;

/**
 * Class Supported Hash Algorithm List Validator
 *
 * Validates that the given list is an array consistent of strings only
 *
 * Also - if the given list's keys are not integers, then this validator will
 * not pass it!
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Hash\Algorithm\Validators
 */
class SupportedHashAlgorithmListValidator extends BaseValidator{

    /**
     * Validate the set validate-value, based upon eventual set validate-
     * options (if supported by concrete validator)
     *
     * @return bool True if the value is valid, false if not
     */
    public function validate() {
        $value = $this->getValidateValue();
        if(!is_array($value)){
            self::$lastErrorMessage = sprintf('%s is not an array', var_export($value, true));
            return false;
        }

        foreach($value as $key => $algorithm){
            if(!is_int($key)){
                self::$lastErrorMessage = sprintf('The given list (%s) of algorithms cannot be given as an associative array!', var_export($value, true));
                return false;
            }
            if(!NonEmptyStringValidator::isValid($algorithm)){
                return false;
            }
        }

        return true;
    }
}