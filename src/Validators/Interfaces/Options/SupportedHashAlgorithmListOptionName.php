<?php  namespace Aedart\Model\Hash\Algorithm\Validators\Interfaces\Options; 

/**
 * Interface Supported Hash Algorithm List Option Name
 *
 * Contains a single const; an option name
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Hash\Algorithm\Validators\Interfaces\Options
 */
interface SupportedHashAlgorithmListOptionName {

    /**
     * Supported Hash Algorithm List - validate option name
     *
     * When provided, a given hash algorithm name is matched against
     * the list of supported algorithms
     */
    const SUPPORTED_HASH_ALGORITHM_LIST = 'supportedHashAlgorithmList';

}