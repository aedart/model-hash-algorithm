<?php  namespace Aedart\Model\Hash\Algorithm\Validators;

use Aedart\Model\Hash\Algorithm\Interfaces\SupportedHashAlgorithmListAware;
use Aedart\Model\Hash\Algorithm\Traits\SupportedHashAlgorithmListTrait;
use Aedart\Model\Hash\Algorithm\Validators\Interfaces\Options\SupportedHashAlgorithmListOptionName;
use Aedart\Validate\BaseValidator;
use Aedart\Validate\String\NonEmptyStringValidator;

/**
 * Class Hash Algorithm Name Validator
 *
 * <br />
 *
 * Validates the given hashing algorithm name against a list of supported
 * algorithms. If the given name is not included in the list, then the
 * validation fails
 *
 * <br />
 *
 * Validator is case-insensitive
 *
 * <br />
 *
 * <b>Supported validation options</b>
 * <pre>
 *  $options = [
 *      HashAlgorithmNameValidator::SUPPORTED_HASH_ALGORITHM_LIST          =>  hash_algos() // Supported Hashing Algorithms list, default PHP's hash_algos()
 *  ];
 * </pre>
 *
 * @see SupportedHashAlgorithmListAware
 * @see hash_algos()
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Hash\Algorithm\Validators
 */
class HashAlgorithmNameValidator extends BaseValidator implements SupportedHashAlgorithmListAware, SupportedHashAlgorithmListOptionName{

    use SupportedHashAlgorithmListTrait;

    /**
     * Validate the set validate-value, based upon eventual set validate-
     * options (if supported by concrete validator)
     *
     * @return bool True if the value is valid, false if not
     */
    public function validate() {
        $value = $this->getValidateValue();
        if(!NonEmptyStringValidator::isValid($value)){
            return false;
        }

        $value = strtolower($value);
        $supportedList = $this->getSupportedHashAlgorithmList();

        foreach($supportedList as $key => $name){
            $supportedAlgorithm = strtolower($name);
            if($value == $supportedAlgorithm){
                return true;
            }
        }

        self::$lastErrorMessage = sprintf('"%s" is not a supported hash algorithm', var_export($value, true));
        return false;
    }
}