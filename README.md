## Model-Hash-Algorithm ##

Getter and Setter package for a model hash algorithm (name).

This package is part of the Aedart\Model namespace; visit https://bitbucket.org/aedart/model to learn more about the project.

Official sub-package website (https://bitbucket.org/aedart/model-hash-algorithm)

## Contents ##

[TOC]

## When to use this ##

When you need to keep track of what hashing algorithm was used

## How to install ##

```
#!console

composer require aedart/model-hash-algorithm 1.*
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Quick start ##

Provided that you have an interface, e.g. for a person, you can extend the hash-algorithm-aware interface;

```
#!php
<?php
use Aedart\Model\Hash\Algorithm\Interfaces\HashAlgorithmAware;

interface IFile extends HashAlgorithmAware {

    // ... Remaining interface implementation not shown ...
    
}

```

In your concrete implementation, you simple use the hash-algorithm-traits;
 
```
#!php
<?php
use Aedart\Model\Hash\Algorithm\Traits\HashAlgorithmTrait;

class MyPicture implements IFile {
 
    use HashAlgorithmTrait;

    // ... Remaining implementation not shown ... 
 
}
```

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package