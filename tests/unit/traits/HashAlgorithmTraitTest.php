<?php

use Aedart\Model\Hash\Algorithm\Interfaces\HashAlgorithmAware;
use Aedart\Model\Hash\Algorithm\Traits\HashAlgorithmTrait;
use Faker\Factory as FakerFactory;

/**
 * Class HashAlgorithmTraitTest
 *
 * @coversDefaultClass Aedart\Model\Hash\Algorithm\Traits\HashAlgorithmTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class HashAlgorithmTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Model\Hash\Algorithm\Interfaces\HashAlgorithmAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Model\Hash\Algorithm\Traits\HashAlgorithmTrait');
        return $m;
    }

    /**
     * Get a dummy implementation
     *
     * @return Aedart\Model\Hash\Algorithm\Interfaces\HashAlgorithmAware
     */
    protected function getDummyClass(){
        return new DummyHashAlgorithmClass();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::getDefaultHashAlgorithm
     * @covers ::getHashAlgorithm
     * @covers ::hasHashAlgorithm
     * @covers ::hasDefaultHashAlgorithm
     */
    public function getDefaultHashAlgorithm(){
        $trait = $this->getTraitMock();
        $this->assertFalse($trait->hasDefaultHashAlgorithm());
        $this->assertNull($trait->getHashAlgorithm());
    }

    /**
     * @test
     * @covers ::getHashAlgorithm
     * @covers ::hasHashAlgorithm
     * @covers ::hasDefaultHashAlgorithm
     * @covers ::setHashAlgorithm
     * @covers ::isHashAlgorithmValid
     * @covers ::getDefaultHashAlgorithm
     */
    public function getCustomDefaultHashAlgorithm(){
        $dummy = $this->getDummyClass();
        $this->assertNotEmpty($dummy->getHashAlgorithm());
    }

    /**
     * @test
     * @covers ::getHashAlgorithm
     * @covers ::hasHashAlgorithm
     * @covers ::hasDefaultHashAlgorithm
     * @covers ::setHashAlgorithm
     * @covers ::isHashAlgorithmValid
     */
    public function setAndGetHashAlgorithm(){
        $trait = $this->getTraitMock();

        $list = hash_algos();
        shuffle($list);
        $name = $list[rand(0, count($list) - 1)];

        $trait->setHashAlgorithm($name);

        $this->assertSame($name, $trait->getHashAlgorithm());
    }

    /**
     * @test
     * @covers ::setHashAlgorithm
     * @covers ::isHashAlgorithmValid
     *
     * @expectedException \Aedart\Model\Hash\Algorithm\Exceptions\InvalidHashAlgorithmException
     */
    public function attemptSetInvalidHashAlgorithm(){
        $trait = $this->getTraitMock();

        $name = $this->faker->word;

        $trait->setHashAlgorithm($name);
    }
}

class DummyHashAlgorithmClass implements HashAlgorithmAware{
    use HashAlgorithmTrait;

    public function getDefaultHashAlgorithm(){
        $list = hash_algos();
        shuffle($list);
        return $list[rand(0, count($list) - 1)];
    }
}