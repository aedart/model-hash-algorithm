<?php

use Aedart\Model\Hash\Algorithm\Interfaces\SupportedHashAlgorithmListAware;
use Aedart\Model\Hash\Algorithm\Traits\SupportedHashAlgorithmListTrait;
use Faker\Factory as FakerFactory;

/**
 * Class SupportedHashAlgorithmListTraitTest
 *
 * @coversDefaultClass Aedart\Model\Hash\Algorithm\Traits\SupportedHashAlgorithmListTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class SupportedHashAlgorithmListTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Model\Hash\Algorithm\Interfaces\SupportedHashAlgorithmListAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Model\Hash\Algorithm\Traits\SupportedHashAlgorithmListTrait');
        return $m;
    }

    /**
     * Get a dummy implementation
     *
     * @return Aedart\Model\Hash\Algorithm\Interfaces\SupportedHashAlgorithmListAware
     */
    protected function getDummyClass(){
        return new DummySupportedHashAlgoListClass();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::getDefaultSupportedHashAlgorithmList
     * @covers ::getSupportedHashAlgorithmList
     * @covers ::hasSupportedHashAlgorithmList
     * @covers ::hasDefaultSupportedHashAlgorithmList
     * @covers ::setSupportedHashAlgorithmList
     * @covers ::isSupportedHashAlgorithmListValid
     */
    public function getDefaultSupportedHashAlgorithmList(){
        $trait = $this->getTraitMock();
        $this->assertNotEmpty($trait->getSupportedHashAlgorithmList());
    }

    /**
     * @test
     * @covers ::getDefaultSupportedHashAlgorithmList
     * @covers ::getSupportedHashAlgorithmList
     * @covers ::hasSupportedHashAlgorithmList
     * @covers ::hasDefaultSupportedHashAlgorithmList
     */
    public function getCustomDefaultSupportedHashAlgorithmList(){
        $dummy = $this->getDummyClass();
        $this->assertEmpty($dummy->getSupportedHashAlgorithmList());
    }

    /**
     * @test
     * @covers ::getSupportedHashAlgorithmList
     * @covers ::hasSupportedHashAlgorithmList
     * @covers ::setSupportedHashAlgorithmList
     * @covers ::isSupportedHashAlgorithmListValid
     */
    public function setAndGetSupportedHashAlgorithmList(){
        $trait = $this->getTraitMock();

        $list = $this->faker->words();

        $trait->setSupportedHashAlgorithmList($list);

        $this->assertSame($list, $trait->getSupportedHashAlgorithmList());
    }

    /**
     * @test
     * @covers ::getSupportedHashAlgorithmList
     * @covers ::hasSupportedHashAlgorithmList
     * @covers ::setSupportedHashAlgorithmList
     * @covers ::isSupportedHashAlgorithmListValid
     *
     * @expectedException \Aedart\Model\Hash\Algorithm\Exceptions\InvalidHashAlgorithmException
     */
    public function attemptSetInvalidSupportedHashAlgorithmList(){
        $trait = $this->getTraitMock();

        $list = [
            $this->faker->word,
            $this->faker->word,
            $this->faker->unique()->word => $this->faker->word,
        ];

        $trait->setSupportedHashAlgorithmList($list);
    }
}

class DummySupportedHashAlgoListClass implements SupportedHashAlgorithmListAware{
    use SupportedHashAlgorithmListTrait;

    public function getDefaultSupportedHashAlgorithmList() {
        return [];
    }


}