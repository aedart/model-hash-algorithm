<?php

use Aedart\Model\Hash\Algorithm\Validators\HashAlgorithmNameValidator;
use Faker\Factory as FakerFactory;

/**
 * Class HashAlgorithmNameValidatorTest
 *
 * @coversDefaultClass Aedart\Model\Hash\Algorithm\Validators\HashAlgorithmNameValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class HashAlgorithmNameValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
    }

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){

        $faker = FakerFactory::create();

        $algorithms = hash_algos();

        $customAllowedList = $faker->words(10);

        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult
             */

            /*
             * Valid results
             */
            [$this->getRandomHashName($algorithms), [], true],
            [$this->getRandomHashName($algorithms), [], true],
            [$this->getRandomHashName($algorithms), [], true],
            [$this->getRandomHashName($algorithms), [], true],
            [$this->getRandomHashName($algorithms), [], true],
            [$this->getRandomHashName($algorithms), [], true],


            /*
             * Invalid results
             */
            [false, [], false],
            ['', [], false],
            [42, [], false],
            [[], [], false],
            ['base555Algorithm', [], false],
            ['base42½', [], false],

            /*
             * With custom algorithms list
             */
            [$this->getRandomHashName($customAllowedList), [HashAlgorithmNameValidator::SUPPORTED_HASH_ALGORITHM_LIST => $customAllowedList], true],
            [$this->getRandomHashName($customAllowedList), [HashAlgorithmNameValidator::SUPPORTED_HASH_ALGORITHM_LIST => $customAllowedList], true],
            [$this->getRandomHashName($customAllowedList), [HashAlgorithmNameValidator::SUPPORTED_HASH_ALGORITHM_LIST => $customAllowedList], true],
            [$faker->address, [HashAlgorithmNameValidator::SUPPORTED_HASH_ALGORITHM_LIST => $customAllowedList], false],
        ];
    }

    /**
     * Returns a random hash algorithm name
     *
     * @param string[] $list
     *
     * @return string
     */
    public function getRandomHashName(array $list = []){
        shuffle($list);
        return $list[mt_rand(0, count($list) - 1)];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     */
    public function isValid($value, $validateOptions, $expectedResult){
        $result = HashAlgorithmNameValidator::isValid($value, $validateOptions);

        $shouldBeValidMsg = 'valid';
        if($expectedResult === false){
            $shouldBeValidMsg = 'invalid';
        }

        $failMsg = sprintf(
            'Failed validating that %s is %s, with the following options %s',
            var_export($value, true),
            $shouldBeValidMsg,
            var_export($validateOptions, true)
        );

        $this->assertSame($expectedResult, $result, $failMsg);
    }

}