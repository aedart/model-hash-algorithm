<?php

use Aedart\Model\Hash\Algorithm\Validators\SupportedHashAlgorithmListValidator;

/**
 * Class SupportedHashAlgorithmListValidatorTest
 *
 * @coversDefaultClass Aedart\Model\Hash\Algorithm\Validators\SupportedHashAlgorithmListValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class SupportedHashAlgorithmListValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult
             */

            /*
             * Valid results
             */
            [[], [], true],
            [['beta34', 'base5Something', 'md5000'], [], true],

            /*
             * Invalid results
             */
            [['alpha' => 12], [], false],
            [['alpha', 'beta', false], [], false],
            [[1, 2, 3], [], false],
            ['', [], false],
            [42, [], false],
            [42.5, [], false],
            [false, [], false],
            [true, [], false],
            [null, [], false],

            /*
             * No options supported by validator
             */
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     */
    public function isValid($value, $validateOptions, $expectedResult){
        $result = SupportedHashAlgorithmListValidator::isValid($value, $validateOptions);

        $shouldBeValidMsg = 'valid';
        if($expectedResult === false){
            $shouldBeValidMsg = 'invalid';
        }

        $failMsg = sprintf(
            'Failed validating that %s is %s, with the following options %s',
            var_export($value, true),
            $shouldBeValidMsg,
            var_export($validateOptions, true)
        );

        $this->assertSame($expectedResult, $result, $failMsg);
    }

}